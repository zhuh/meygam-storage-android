package com.meygam.cloudstorage.s3.apps;

import com.google.analytics.tracking.android.ExceptionParser;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Created by Elizabeth on 5/23/2014.
 */
public class AnalyticsExceptionParser implements ExceptionParser {

    @Override
    public String getDescription(String p_thread, Throwable p_throwable) {
        UtilFunctions.sendExceptionMail("Thread: " + p_thread + ", Exception: " + ExceptionUtils.getStackTrace(p_throwable));
        return "Thread: " + p_thread + ", Exception: " + ExceptionUtils.getStackTrace(p_throwable);
    }

}
