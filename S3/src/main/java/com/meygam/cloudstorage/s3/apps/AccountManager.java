package com.meygam.cloudstorage.s3.apps;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Elizabeth on 5/8/2014.
 */
public class AccountManager {
    private AccountDB accountDB = AccountDB.getInstance();
    private static AccountManager instance = new AccountManager();
    public static final String ACCOUNT_LIST_FILE = "accountList.json";

    GsonBuilder gsonBuilder = new GsonBuilder();
    private Gson gson = gsonBuilder.create();

    private String accountListJson;

    public static AccountManager getInstance() {
        return instance;
    }

    public boolean fileExistance(Context context){
        File file1 = context.getFileStreamPath(ACCOUNT_LIST_FILE);
        return file1.exists();
    }

    public void writeToFile(Context context) {
        FileOutputStream fileOutputStream;

        try {
            accountListJson = gson.toJson(accountDB.getAccountList());
            fileOutputStream = context.openFileOutput(ACCOUNT_LIST_FILE,
                    Context.MODE_PRIVATE);
            fileOutputStream.write(accountListJson.getBytes());
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public AccountList readFromFile(Context context) {
        AccountList accountList = null;
        if(!fileExistance(context)) {
            return accountList;
        }
        try {
            FileInputStream fileInputStream
                    = context.openFileInput(AccountManager.ACCOUNT_LIST_FILE);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                    fileInputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            //Type collectionType = new TypeToken<List<ReminderEntry>>(){}.getType();
            accountList = gson.fromJson(stringBuilder.toString(),
                    AccountList.class);
            accountDB.setAccountList(accountList);

            fileInputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return accountList;
    }

    public AccountList addAccount(Context context, String accessKey, String secretKey,
                                  String description, String provider) {
        AccountList accountList = accountDB.addToAccountList(accessKey, secretKey, description,
                provider);
        writeToFile(context);
        return accountList;
    }

    public AccountList removeAccount(Context context, String accessKey, String secretKey,
                                     String description, String provider) {
        AccountList accountList = accountDB.removeFromAccountList(accessKey, secretKey, description,
                provider);
        writeToFile(context);
        return accountList;
    }
}
