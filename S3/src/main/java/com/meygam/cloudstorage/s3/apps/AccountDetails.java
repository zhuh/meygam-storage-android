package com.meygam.cloudstorage.s3.apps;

/**
 * Created by Elizabeth on 5/8/2014.
 */
public class AccountDetails {
    private String accessKey;
    private String secretKey;
    private String description;
    private String provider;

    public AccountDetails(String accessKey, String secretKey, String description, String provider) {
        this.accessKey = accessKey;
        this.secretKey = secretKey;
        this.description = description;
        this.provider = provider;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountDetails that = (AccountDetails) o;

        if (accessKey != null ? !accessKey.equals(that.accessKey) : that.accessKey != null)
            return false;
        if (description != null ? !description.equals(that.description) : that.description != null)
            return false;
        if (provider != null ? !provider.equals(that.provider) : that.provider != null)
            return false;
        if (secretKey != null ? !secretKey.equals(that.secretKey) : that.secretKey != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = accessKey != null ? accessKey.hashCode() : 0;
        result = 31 * result + (secretKey != null ? secretKey.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (provider != null ? provider.hashCode() : 0);
        return result;
    }
}

