package com.meygam.cloudstorage.s3.apps;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.meygam.cloudstorage.R;

import java.util.ArrayList;

/**
 * Created by Elizabeth on 5/7/2014.
 */
public class StorageDisplayAdapter extends ArrayAdapter<StorageDisplay> {
    private ArrayList<StorageDisplay> storageDisplayArrayList;

    public StorageDisplayAdapter(Context context, int resource, ArrayList<StorageDisplay> objects) {
        super(context, resource, objects);
        this.storageDisplayArrayList = objects;
    }

    @Override
    public int getCount() {
        return storageDisplayArrayList.size();
    }

    @Override
    public StorageDisplay getItem(int position) {
        return storageDisplayArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_row, parent, false);
        }

        StorageDisplay storageDisplay = storageDisplayArrayList.get(position);
        if(storageDisplay != null) {
            TextView description = (TextView) v.findViewById(R.id.description);
            TextView numBuckets = (TextView) v.findViewById(R.id.numBuckets);

            if(description != null) {
                description.setText(storageDisplay.getDescription());
            }

            if(numBuckets != null) {
                numBuckets.setText(storageDisplay.getNumBuckets());
            }
        }
        return v;
    }
}
