package com.meygam.cloudstorage.s3.apps;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;


public class SettingsActivity extends PreferenceActivity {

    public static final String KEY_DOWNLOAD_DIR = "downloadDirKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    public static class SettingsFragment extends PreferenceFragment
            implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                    = Thread.getDefaultUncaughtExceptionHandler();
            if (uncaughtExceptionHandler instanceof ExceptionReporter) {
                ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
                exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
            }

            addPreferencesFromResource(R.xml.preferences);

            if(!PreferenceManager.getDefaultSharedPreferences(
                    getActivity())
                    .getBoolean(PreferenceManager.KEY_HAS_SET_DEFAULT_VALUES, false)) {
                PreferenceManager.setDefaultValues(getActivity(),
                        R.xml.preferences, false);
            }

            findPreference(KEY_DOWNLOAD_DIR)
                    .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent nextActivity = new Intent(getActivity(), SelectDirectoryActivity.class);
                    startActivity(nextActivity);
                    return true;
                }
            });

            findPreference("feedbackPrefKey")
                    .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(Preference arg0) {
                            //code for what you want it to do
                            Intent nextActivity = new Intent(getActivity(), FeedbackActivity.class);
                            startActivity(nextActivity);
                            return true;
                }
            });

            findPreference("faqPrefKey")
                    .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                        @Override
                        public boolean onPreferenceClick(Preference arg0) {
                            //code for what you want it to do
                            Intent nextActivity = new Intent(getActivity(), FaqActivity.class);
                            startActivity(nextActivity);
                            return true;
                }
            });
        }

        @Override
        public void onStart() {
            super.onStart();
            SharedPreferences sharedPreferences = getActivity()
                    .getSharedPreferences("myPrefs", MODE_PRIVATE);
            String defaultValue = getActivity().getResources()
                    .getString(R.string.default_download_dir);
            String downloadDir = sharedPreferences.getString(KEY_DOWNLOAD_DIR, defaultValue);

            if(!downloadDir.equals("")) {
                String title = "Download Directory : " + downloadDir;
                findPreference(KEY_DOWNLOAD_DIR).setTitle(title);
            }
        }

        @Override
        public void onResume() {
            super.onResume();
            PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext())
                    .registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onPause() {
            super.onPause();
            PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext())
                    .unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {

        }
    }
}
