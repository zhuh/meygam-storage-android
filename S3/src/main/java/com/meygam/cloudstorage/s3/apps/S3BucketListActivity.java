package com.meygam.cloudstorage.s3.apps;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.amazonaws.AmazonClientException;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;

import java.util.List;


public class S3BucketListActivity extends CustomListActivity implements SwipeRefreshLayout.OnRefreshListener {
    protected String accountName;
    protected List<String> bucketNameList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        Bundle extras = this.getIntent().getExtras();
        accountName = extras.getString(S3.ACCOUNT_NAME);
        startPopulateList();
    }

    protected void obtainListItems() {
        new GetBucketNamesTask().execute();
    }

    @Override
    protected void wireOnListClick() {
        getItemList().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> list, View view,
                                    int position, long id) {
                final String bucketName = ((TextView) view).getText()
                        .toString();
                Intent bucketViewIntent = new Intent(S3BucketListActivity.this,
                        S3BucketViewActivity.class);
                bucketViewIntent.putExtra(S3.ACCOUNT_NAME, accountName);
                bucketViewIntent.putExtra(S3.BUCKET_NAME, bucketName);
                bucketViewIntent.putExtra(S3.PREFIX, "/");
                bucketViewIntent.putExtra(S3.FOLDER_NAME, "");
                startActivity(bucketViewIntent);
                overridePendingTransition(R.anim.animation_slide_in_left,
                        R.anim.animation_slide_out_left);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.animation_slide_in_right, R.anim.animation_slide_out_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.s3_bucket_list, menu);
        return true;
    }

    public boolean onSettingsClicked(MenuItem item) {
        Intent nextActivity = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(nextActivity);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                onSettingsClicked(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected class GetBucketNamesTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                bucketNameList = S3.getBucketNames(accountName);
            } catch (final AmazonClientException exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(S3BucketListActivity.this);
                        builder.setTitle("Amazon Exception")
                                .setMessage(exception.getMessage())
                                .setIcon(R.drawable.ic_alert_icon)
                                .setPositiveButton("OK", null);
                        builder.create().show();
                    }
                });
                return null;
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            updateUi(bucketNameList);
        }
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                startPopulateList();
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 2000);
    }
}
