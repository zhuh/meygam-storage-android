package com.meygam.cloudstorage.s3.apps;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.model.S3Object;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;


public class S3ObjectViewActivity extends Activity {
    protected TextView bodyText;
    protected String accountName;
    protected String bucketName;
    protected String objectName;
    protected String folderName;
    protected InputStream objectData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_view);

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        Bundle extras = this.getIntent().getExtras();
        accountName = extras.getString(S3.ACCOUNT_NAME);
        bucketName = extras.getString(S3.BUCKET_NAME);
        objectName = extras.getString(S3.OBJECT_NAME);
        folderName = extras.getString(S3.FOLDER_NAME);
        bodyText = (TextView) findViewById(R.id.item_view_body_text);
        startPopulateText();
    }

    private void startPopulateText(){
        new GetDataForObjectTask().execute();
    }

    private void updateUi(){
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.animation_slide_in_right,
                R.anim.animation_slide_out_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.s3_bucket_list, menu);
        return true;
    }

    public boolean onSettingsClicked(MenuItem item) {
        Intent nextActivity = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(nextActivity);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_settings:
                onSettingsClicked(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String getExt(String filename)
    {
        int dotIndex = 0;
        for(int i = filename.length()-1; i >= 0; i--)
        {
            if(filename.charAt(i) == '.')
            {
                dotIndex = i;
                break;
            }
        }
        return filename.substring(dotIndex + 1, filename.length());
    }

    public String getTypeAction(String s)
    {
        int slashIndex = 0;
        for(int i = 0; i < s.length(); i++)
        {
            if(s.charAt(i) == '/')
            {
                slashIndex = i;
                break;
            }
        }
        return s.substring(0, slashIndex);
    }

    private class GetDataForObjectTask extends AsyncTask<Void, String, Void> {
        private long lengthOfFile = 0;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(S3ObjectViewActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        protected Void doInBackground(Void... voids) {

            try {
                S3Object s3Object = S3.getS3Object(accountName, bucketName, folderName);
                objectData = s3Object.getObjectContent();
                lengthOfFile = s3Object.getObjectMetadata().getContentLength();
            } catch (final AmazonClientException exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(S3ObjectViewActivity.this);
                        builder.setTitle("Amazon Exception")
                                .setMessage(exception.getMessage())
                                .setIcon(R.drawable.ic_alert_icon)
                                .setPositiveButton("OK", null);
                        builder.create().show();
                    }
                });
                return null;
            }

            byte[] buffer = new byte[1024];
            File file = new File(getApplicationContext().getExternalFilesDir(null), objectName);
            try {
                OutputStream outputStream
                        = new FileOutputStream(file);
                int count = 0;
                long total = 0;
                while ((count = objectData.read(buffer)) != -1) {
                    total+= count;
                    if (Thread.interrupted()) {
                        throw new InterruptedException();
                    }
                    publishProgress(""+(int)((total*100)/lengthOfFile));
                    outputStream.write(buffer, 0, count);
                }
                outputStream.close();
                objectData.close();

            } catch (final Exception exception) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(S3ObjectViewActivity.this);
                        builder.setTitle("Exception")
                                .setMessage(exception.getMessage())
                                .setIcon(R.drawable.ic_alert_icon)
                                .setPositiveButton("OK", null);
                        builder.create().show();
                    }
                });
                return null;
            }

            MimeTypeMap mime = MimeTypeMap.getSingleton();
            String ext = getExt(objectName).toLowerCase(Locale.ENGLISH);
            String type = mime.getMimeTypeFromExtension(ext);

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            if(type == null) {
                intent.setDataAndType(Uri.fromFile(file), "*/*");
            } else
                intent.setDataAndType(Uri.fromFile(file), getTypeAction(type) + "/*");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            startActivity(intent);
            overridePendingTransition(R.anim.animation_slide_in_left,
                    R.anim.animation_slide_out_left);
            UtilFunctions.sendEventAnalytics(getApplicationContext(),
                    "Meygam Storage - S3ObjectViewActivity",
                    "View Object",
                    "Processed");

            return null;
        }

        protected void onProgressUpdate(String... values) {
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

        protected void onPostExecute(Void result) {
            progressDialog.dismiss();
            updateUi();
        }
    }
}
