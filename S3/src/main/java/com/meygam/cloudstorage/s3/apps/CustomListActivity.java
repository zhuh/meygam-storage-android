package com.meygam.cloudstorage.s3.apps;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;

import java.util.List;


public abstract class CustomListActivity extends AlertActivity implements SwipeRefreshLayout.OnRefreshListener {
    protected ListView itemList;
    protected Button moreButton;
    protected ArrayAdapter<String> itemListAdapter;
    protected SwipeRefreshLayout swipeRefreshLayout;

    public static final int LEFT = 0;
    public static final int CENTER = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_list);

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        itemList = (ListView) findViewById(R.id.item_list_view);
        moreButton = (Button) findViewById(R.id.item_list_more_button);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        itemList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                boolean enable = false;
                if(itemList != null && itemList.getChildCount() > 0){
                    // check if the first item of the list is visible
                    boolean firstItemVisible = itemList.getFirstVisiblePosition() == 0;
                    // check if the top of the first item is visible
                    boolean topOfFirstItemVisible = itemList.getChildAt(0).getTop() == 0;
                    // enabling or disabling the refresh layout
                    enable = firstItemVisible && topOfFirstItemVisible;
                }
                swipeRefreshLayout.setEnabled(enable);
            }
        });

    }

    protected void updateUi(String[] list, String successMessage) {
        updateUi(list, LEFT);
    }

    protected void updateUi(List<String> list) {
        updateUi(list, LEFT);
    }

    protected void updateUi(List<String> list, int justify) {
        if (justify == LEFT) {
            itemListAdapter = new ArrayAdapter<String>(this, R.layout.row_left,
                    list);
        } else if (justify == CENTER) {
            itemListAdapter = new ArrayAdapter<String>(this, R.layout.row, list);
        }
        itemList.setAdapter(itemListAdapter);
        itemListAdapter.notifyDataSetChanged();
        wireOnListClick();
    }

    protected void updateUi(String[] list, int justify) {
        if (justify == LEFT) {
            itemListAdapter = new ArrayAdapter<String>(this, R.layout.row_left);
        } else if (justify == CENTER) {
            itemListAdapter = new ArrayAdapter<String>(this, R.layout.row);
        }
        itemList.setAdapter(itemListAdapter);
        for (String item : list) {
            itemListAdapter.add(item);
        }
        itemListAdapter.notifyDataSetChanged();
        wireOnListClick();
    }

    protected void updateList(String[] itemNameList) {
        for (String item : itemNameList) {
            itemListAdapter.add(item);
        }
        itemListAdapter.notifyDataSetChanged();
        wireOnListClick();
    }

    protected void updateList(List<String> itemNameList) {
        if (itemNameList.size() == 0) {
            disablePagination();
        } else {
            enablePagination();
            for (String item : itemNameList) {
                itemListAdapter.add(item);
            }
            itemListAdapter.notifyDataSetChanged();
            wireOnListClick();
        }
    }

    protected void startPopulateList() {
        obtainListItems();
    }

    protected void getMoreItems() {
        moreButton.setOnClickListener(null);
        obtainMoreItems();
    }

    protected abstract void obtainListItems();

    protected void obtainMoreItems() {
        return;
    }

    public ListView getItemList() {
        return itemList;
    }

    public void enablePagination() {
        moreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMoreItems();
            }
        });
        moreButton.setVisibility(View.VISIBLE);
    }

    public void disablePagination() {
        moreButton.setOnClickListener(null);
        moreButton.setVisibility(View.INVISIBLE);
    }

    protected void wireOnListClick() {
        return;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
