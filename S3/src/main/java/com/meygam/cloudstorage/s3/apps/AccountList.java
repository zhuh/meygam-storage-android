package com.meygam.cloudstorage.s3.apps;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Elizabeth on 5/9/2014.
 */
public class AccountList extends ArrayList<AccountDetails> {
    public AccountList(Collection<? extends AccountDetails> collection) {
        super(collection);
    }

    public AccountList() {}

}
