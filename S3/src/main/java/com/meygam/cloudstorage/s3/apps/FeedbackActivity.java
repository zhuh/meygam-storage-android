package com.meygam.cloudstorage.s3.apps;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.meygam.cloudstorage.R;

public class FeedbackActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);

        Thread.UncaughtExceptionHandler uncaughtExceptionHandler
                = Thread.getDefaultUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof ExceptionReporter) {
            ExceptionReporter exceptionReporter = (ExceptionReporter) uncaughtExceptionHandler;
            exceptionReporter.setExceptionParser(new AnalyticsExceptionParser());
        }

        final EditText feedbackEditText = (EditText) findViewById(R.id.feedback_edit_text);
        final Button submit = (Button) findViewById(R.id.feedback_btn_submit);

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean validationSuccess = true;
                String reportMessage = "";
                reportMessage += feedbackEditText.getText() + "\n";

                if (feedbackEditText.length() == 0) {
                    // no feedback entered
                    Toast toast = Toast.makeText(getApplicationContext(),
                            "Please enter your comments.", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                    validationSuccess = false;
                }

                if (validationSuccess == true) {
                    UtilFunctions.sendFeedback(reportMessage);
                    Toast.makeText(getApplicationContext(), "Thanks for your feedback and time",
                            Toast.LENGTH_LONG).show();
                    FeedbackActivity.this.finish();
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }
}


