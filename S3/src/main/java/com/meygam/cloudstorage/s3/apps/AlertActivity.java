package com.meygam.cloudstorage.s3.apps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;

import com.meygam.cloudstorage.R;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;


public class AlertActivity extends Activity {
    private String errorTrace;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    protected Runnable displayError = new Runnable(){
        public void run(){
            AlertDialog.Builder confirm = new AlertDialog.Builder( AlertActivity.this );
            confirm.setTitle( "A Connection Error Occured!");
            confirm.setMessage( "Please Review the README\n" + errorTrace );
            confirm.setNegativeButton( "OK", new DialogInterface.OnClickListener() {
                public void onClick( DialogInterface dialog, int which ) {
                    AlertActivity.this.finish();
                }
            } );
            confirm.show().show();
        }
    };

    protected void setStackTrace(Throwable aThrowable) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        aThrowable.printStackTrace(printWriter);
        errorTrace = result.toString();
    }

    public void setStackAndPost(Throwable aThrowable){
        setStackTrace(aThrowable);
        mHandler.post(displayError);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.alert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
